package com.example.avhedo.mappers.infoUserMappers;

import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import com.example.avhedo.models.userModels.InfoUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface InfoUserTwoMapper {
    @Mapping(source = "userInfo", target = "userInfo")
    InfoUserFromLentaDto toDTO(InfoUser infoUser);
}
