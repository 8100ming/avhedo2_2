package com.example.avhedo.mappers.infoUserMappers;

import com.example.avhedo.dto.infoAllUserDto.InfoUserDto;
import com.example.avhedo.models.userModels.InfoUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface InfoUserMapper {
    @Mapping(source = "userInfo", target = "userDto")
    InfoUserDto toDTO(InfoUser infoUser);
}
