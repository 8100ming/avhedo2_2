package com.example.avhedo.mappers.infoUserMappers;

import com.example.avhedo.dto.onlineDatingAllDto.OnlineDatingDto;
import com.example.avhedo.models.OnlineDatingPeople;
import com.example.avhedo.models.userModels.InfoUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OnlineDatingInfoUserMappers {
    @Mapping(source = "infoUser.userInfo", target = "userDto")
    @Mapping(source = "onlineDatingPeople.mutualLike", target = "mutualLike")
    OnlineDatingDto toDTO(InfoUser infoUser, OnlineDatingPeople onlineDatingPeople);
}
