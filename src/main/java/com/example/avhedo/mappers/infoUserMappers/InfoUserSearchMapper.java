package com.example.avhedo.mappers.infoUserMappers;

import com.example.avhedo.dto.userAllDto.SearchUserDto;
import com.example.avhedo.models.userModels.InfoUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface InfoUserSearchMapper {
    @Mapping(source = "userInfo", target = "userDto")
    SearchUserDto toDTO(InfoUser infoUser);
}
