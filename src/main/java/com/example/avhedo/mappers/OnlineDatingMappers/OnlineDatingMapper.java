package com.example.avhedo.mappers.OnlineDatingMappers;

import com.example.avhedo.dto.onlineDatingAllDto.OnlineDatingDtoSmall;
import com.example.avhedo.models.OnlineDatingPeople;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OnlineDatingMapper {
    OnlineDatingDtoSmall toDTO(OnlineDatingPeople onlineDatingPeople);
}
