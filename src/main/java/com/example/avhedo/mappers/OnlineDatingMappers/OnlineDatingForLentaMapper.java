package com.example.avhedo.mappers.OnlineDatingMappers;

import com.example.avhedo.dto.onlineDatingAllDto.OnlineDatingDtoForLenta;
import com.example.avhedo.models.userModels.InfoUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OnlineDatingForLentaMapper {
    @Mapping(source = "userInfo", target = "userDto")
    OnlineDatingDtoForLenta toDTO(InfoUser infoUser);
}
