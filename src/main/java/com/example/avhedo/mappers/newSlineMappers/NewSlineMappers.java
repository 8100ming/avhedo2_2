package com.example.avhedo.mappers.newSlineMappers;

import com.example.avhedo.dto.newSline.NewSlineDto;
import com.example.avhedo.models.newSlineModels.NewSline;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NewSlineMappers {
    @Mapping(source = "userNewSline", target = "userNewSline")
    @Mapping(source = "commentsList", target = "commentsList")
    @Mapping(source = "likesNewSlines", target = "likesNewSlines")
    NewSlineDto toDTO(NewSline newSline);
}
