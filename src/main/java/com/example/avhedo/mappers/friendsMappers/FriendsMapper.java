package com.example.avhedo.mappers.friendsMappers;

import com.example.avhedo.dto.FriendsDto;
import com.example.avhedo.models.Friends;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FriendsMapper {
    //@Mapping(source = "pageOwners",target = "ownerPage")
    @Mapping(source = "friends", target = "friends")
    FriendsDto toDTO(Friends friends);

}
