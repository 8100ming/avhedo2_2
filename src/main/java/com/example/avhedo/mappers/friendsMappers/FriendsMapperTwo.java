package com.example.avhedo.mappers.friendsMappers;

import com.example.avhedo.dto.FriendsDto;
import com.example.avhedo.models.Friends;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FriendsMapperTwo {
    @Mapping(source = "pageOwners", target = "friends")
    FriendsDto toDTO(Friends friends);
}
