package com.example.avhedo.models.userModels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class InfoUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String dateOfBirthday;
    private String aboutMe;
    private String hobby;
    private String url;
    private String sex;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User userInfo;

    public InfoUser(String dateOfBirthday, String aboutMe, String hobby, String url, String sex, User userInfo) {
        this.dateOfBirthday = dateOfBirthday;
        this.aboutMe = aboutMe;
        this.hobby = hobby;
        this.url = url;
        this.sex = sex;
        this.userInfo = userInfo;
    }

    public InfoUser(Long id, String dateOfBirthday, String aboutMe, String hobby, String url, String sex, User userInfo) {
        this.id = id;
        this.dateOfBirthday = dateOfBirthday;
        this.aboutMe = aboutMe;
        this.hobby = hobby;
        this.url = url;
        this.sex = sex;
        this.userInfo = userInfo;
    }
}
