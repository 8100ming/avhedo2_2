package com.example.avhedo.models;

import com.example.avhedo.models.userModels.InfoUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Friends {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Owner_id")
    private InfoUser pageOwners;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friends_id")
    private InfoUser friends;

    public Friends(InfoUser pageOwners, InfoUser friends) {
        this.pageOwners = pageOwners;
        this.friends = friends;
    }
}
