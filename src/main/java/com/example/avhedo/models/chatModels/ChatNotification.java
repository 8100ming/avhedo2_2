package com.example.avhedo.models.chatModels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class ChatNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String senderId;
    private String senderName;
    private String recipientId;

    public ChatNotification(String senderId, String senderName, String recipientId) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.recipientId = recipientId;
    }
}
