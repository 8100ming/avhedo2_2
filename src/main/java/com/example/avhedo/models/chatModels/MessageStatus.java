package com.example.avhedo.models.chatModels;

public enum MessageStatus {
    RECEIVED, DELIVERED
}
