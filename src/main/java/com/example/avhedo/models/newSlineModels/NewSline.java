package com.example.avhedo.models.newSlineModels;

import com.example.avhedo.models.userModels.InfoUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "NewSline")
public class NewSline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String textMessage;
    private String titleMessage;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private InfoUser userNewSline;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Comments> commentsList;
    @OneToMany(cascade = CascadeType.ALL)
    private List<LikesNewSline> likesNewSlines;

    public NewSline(String textMessage, String titleMessage, InfoUser userNewSline, List<Comments> commentsList, List<LikesNewSline> likesNewSlines) {
        this.textMessage = textMessage;
        this.titleMessage = titleMessage;
        this.userNewSline = userNewSline;
        this.commentsList = commentsList;
        this.likesNewSlines = likesNewSlines;
    }
}
