package com.example.avhedo.models.newSlineModels;

import com.example.avhedo.models.userModels.InfoUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Comments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String textComments;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private InfoUser userMessage;

    public Comments(String textComments, InfoUser userMessage) {
        this.textComments = textComments;
        this.userMessage = userMessage;
    }
}
