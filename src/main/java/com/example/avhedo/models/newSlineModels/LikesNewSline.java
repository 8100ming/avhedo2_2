package com.example.avhedo.models.newSlineModels;

import com.example.avhedo.models.userModels.InfoUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class LikesNewSline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean likesMessage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private InfoUser userWhichLikes;


    public LikesNewSline(Boolean likesMessage, InfoUser userWhichLikes) {
        this.likesMessage = likesMessage;
        this.userWhichLikes = userWhichLikes;
    }
}
