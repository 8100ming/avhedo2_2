package com.example.avhedo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@Entity
@Getter
@Setter
public class OnlineDatingPeople {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idPeopleWhoLove;
    private Long idPeopleWichLove;
    private Boolean mutualLike;

    public OnlineDatingPeople(Long idPeopleWhoLove, Long idPeopleWichLove, Boolean mutualLike) {
        this.idPeopleWhoLove = idPeopleWhoLove;
        this.idPeopleWichLove = idPeopleWichLove;
        this.mutualLike = mutualLike;
    }
}
