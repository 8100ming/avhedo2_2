package com.example.avhedo.services.userServices;

import com.example.avhedo.models.userModels.ERole;
import com.example.avhedo.models.userModels.Role;
import com.example.avhedo.repositories.userRepositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleServices {
    private final RoleRepository roleRepository;

    public Optional<Role> findByName(ERole name) {
        return roleRepository.findByName(name);
    }
}
