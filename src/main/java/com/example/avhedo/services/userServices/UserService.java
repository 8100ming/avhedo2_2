package com.example.avhedo.services.userServices;

import com.example.avhedo.configs.jwt.JwtUtils;
import com.example.avhedo.dto.authorizeDto.LoginRequest;
import com.example.avhedo.dto.authorizeDto.SignupRequest;
import com.example.avhedo.dto.userAllDto.UserDetailsDto;
import com.example.avhedo.models.exception.EmailExistsException;
import com.example.avhedo.models.exception.UsernameExistsException;
import com.example.avhedo.models.userModels.ERole;
import com.example.avhedo.models.userModels.Role;
import com.example.avhedo.models.userModels.User;
import com.example.avhedo.repositories.userRepositories.UserRepository;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleServices roleServices;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private static final String moderator = "mod";
    private static final String admin = "admin";

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void checkIfEmailExists(String email) {
        if (existsByEmail(email)) {
            throw new EmailExistsException("Пользователь с такой почтой существует: " + email);
        }
    }

    public void checkIfUsernameExists(String username) {
        if (existsByUsername(username)) {
            throw new UsernameExistsException("Пользователь с таким ником существует: " + username);
        }
    }
    @Transactional
    public User saveUser(SignupRequest signupRequest) {
        checkIfEmailExists(signupRequest.getEmail());
        checkIfUsernameExists(signupRequest.getUsername());
        User user = new User(signupRequest.getUsername(),
                signupRequest.getEmail(),
                passwordEncoder.encode(signupRequest.getPassword()));
        Set<String> reqRoles = signupRequest.getRoles();
        Set<Role> roles = new HashSet<>();
        roleVerification(reqRoles, roles);
        user.setRoles(roles);
        save(user);
        return user;
    }

    public UserDetailsDto signInUser(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        return new UserDetailsDto(jwt, userDetails, roles);
    }


    public void roleVerification(Set<String> reqRoles, Set<Role> roles) {
        if (reqRoles == null) {
            Role userRole = roleServices
                    .findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error, Role USER is not found"));
            roles.add(userRole);
        } else {
            reqRoles.forEach(r -> {
                switch (r) {

                    case admin:
                        Role adminRole = roleServices
                                .findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error, Role ADMIN is not found"));
                        roles.add(adminRole);

                        break;
                    case moderator:
                        Role modRole = roleServices
                                .findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error, Role MODERATOR is not found"));
                        roles.add(modRole);

                        break;

                    default:
                        Role userRole = roleServices
                                .findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error, Role USER is not found"));
                        roles.add(userRole);
                }
            });
        }
    }
    public static List<SignupRequest> convertJsonToList(String json) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<SignupRequest>>(){}.getType();
        return gson.fromJson(json, listType);
    }
}
