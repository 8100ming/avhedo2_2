package com.example.avhedo.services.chatMessageServices;

import com.example.avhedo.dto.ChatMessagesDTO;
import com.example.avhedo.models.chatModels.ChatMessage;
import com.example.avhedo.repositories.chatRepositories.ChatMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ChatMessageService {
    private final ChatMessageRepository chatMessageRepository;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    @Value("${sql.getAllChatUser}")
    private String getAllChatUserSql;
    @Value("${sql.findAllByChatId}")
    private String findAllByChatId;

    public List<ChatMessage> findAllBySenderIdAndAndRecipientId(String senderId, String recipientId) {
        return chatMessageRepository.findAllBySenderIdAndAndRecipientId(senderId, recipientId)
                .stream()
                .collect(Collectors.toList());
    }

    public List<ChatMessagesDTO> getAllChatUser(Long id) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(getAllChatUserSql)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", String.valueOf(id));
        return jdbcTemplate.query(sql,params,(resultSet, rowNum) -> {
            ChatMessagesDTO chatMessagesDTO = new ChatMessagesDTO();
            chatMessagesDTO.setChatId(resultSet.getString("chatId"));
            chatMessagesDTO.setSenderId(resultSet.getString("senderId"));
            chatMessagesDTO.setRecipientId(resultSet.getString("recipientId"));
            chatMessagesDTO.setContent(resultSet.getString("content"));
            chatMessagesDTO.setIdLastSender(resultSet.getString("lastSenderId"));
            chatMessagesDTO.setSenderUrl(resultSet.getString("urlSender"));
            chatMessagesDTO.setRecipientName(resultSet.getString("recipientName"));
            chatMessagesDTO.setSenderName(resultSet.getString("sendername"));
            return chatMessagesDTO;
        });
    }

    public List<ChatMessage> findAllByChatId(String senderId,
                                             String recipientId) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(findAllByChatId)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("senderId",senderId);
        params.addValue("recipientId",recipientId);
        return jdbcTemplate.query(sql, params, (resultSet, rowNum) -> {
            ChatMessage resultChatMessage = new ChatMessage();
            resultChatMessage.setId(resultSet.getLong("id"));
            resultChatMessage.setChatId(resultSet.getString("chat_id"));
            resultChatMessage.setSenderId(resultSet.getString("sender_id"));
            resultChatMessage.setRecipientId(resultSet.getString("recipient_id"));
            resultChatMessage.setSenderName(resultSet.getString("sender_name"));
            resultChatMessage.setRecipientName(resultSet.getString("recipient_name"));
            resultChatMessage.setContent(resultSet.getString("content"));
            resultChatMessage.setTimestamp(resultSet.getTimestamp("timestamp"));
            return resultChatMessage;
        });
    }

    public Optional<ChatMessage> findById(Long id) {
        return chatMessageRepository.findById(id);
    }

}
