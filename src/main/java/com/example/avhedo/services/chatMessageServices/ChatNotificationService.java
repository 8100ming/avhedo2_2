package com.example.avhedo.services.chatMessageServices;

import com.example.avhedo.models.chatModels.ChatMessage;
import com.example.avhedo.models.chatModels.ChatNotification;
import com.example.avhedo.repositories.chatRepositories.ChatMessageRepository;
import com.example.avhedo.repositories.chatRepositories.ChatNotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChatNotificationService {
    private final ChatNotificationRepository chatNotificationRepository;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ChatMessageRepository chatMessageRepository;
    @Value("${sql.deleteChatNotification}")
    private String deleteChatNotification;

    public List<ChatNotification> findAllByRecipientId(String recipientId) {
        return chatNotificationRepository.findAllByRecipientId(recipientId);
    }

    public void deleteNotification(String recipientId, String senderId) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(deleteChatNotification)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("recipientId",recipientId);
        params.addValue("senderId",senderId);
        jdbcTemplate.update(sql,params);
    }

    @Transactional
    public void saveChatNotificationAndMessage(ChatMessage chatMessage) {
        ChatNotification chatNotification = new ChatNotification(chatMessage.getSenderId(), chatMessage.getSenderName(), chatMessage.getRecipientId());
        chatNotificationRepository.save(chatNotification);
        chatMessageRepository.save(chatMessage);
    }

}
