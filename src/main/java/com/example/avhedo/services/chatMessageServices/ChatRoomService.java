package com.example.avhedo.services.chatMessageServices;

import com.example.avhedo.models.chatModels.ChatMessage;
import com.example.avhedo.models.chatModels.ChatNotification;
import com.example.avhedo.models.chatModels.ChatRoom;
import com.example.avhedo.repositories.chatRepositories.ChatRoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class ChatRoomService {
    private final ChatRoomRepository chatRoomRepository;
    @Value("${sql.saveChatRoom}")
    private String saveChatRoom;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ChatNotificationService chatNotificationService;
    private final SimpMessagingTemplate messagingTemplate;

    public void findChatRoom(String recipientId, String senderId, ChatMessage chatMessage) {
        ChatRoom chat = chatRoomRepository.findAllByRecipientIdAndSenderId(recipientId, senderId).get();
        chatMessage.setChatId(chat.getChatId());
    }

    public void saveChatRoom(ChatMessage chatMessage) throws IOException {
        String chatId = String.format("%s_%s", chatMessage.getSenderId(), chatMessage.getRecipientId());
        String sql = new String(Files.readAllBytes(Paths.get(saveChatRoom)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("chatId",chatId);
        params.addValue("senderId",Long.parseLong(chatMessage.getSenderId()));
        params.addValue("recipientId",Long.parseLong(chatMessage.getRecipientId()));
        params.addValue("chatId",chatId);
        params.addValue("recipientId",Long.parseLong(chatMessage.getRecipientId()));
        params.addValue("senderId",Long.parseLong(chatMessage.getSenderId()));
        jdbcTemplate.update(sql,params);
    }
    public void processMessages(ChatMessage chatMessage) throws IOException {
        try {
            findChatRoom(chatMessage.getRecipientId(), chatMessage.getSenderId(), chatMessage);
        } catch (NoSuchElementException e) {
            saveChatRoom(chatMessage);
        }
        chatNotificationService.saveChatNotificationAndMessage(chatMessage);
        messagingTemplate.convertAndSendToUser(
                chatMessage.getRecipientId(), "/queue/messages",
                new ChatNotification(chatMessage.getId(), chatMessage.getSenderId(), chatMessage.getSenderName(), chatMessage.getRecipientId()));
    }


}
