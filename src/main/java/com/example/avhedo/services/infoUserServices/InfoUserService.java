package com.example.avhedo.services.infoUserServices;

import com.example.avhedo.dto.infoAllUserDto.InfoUserDto;
import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import com.example.avhedo.dto.userAllDto.SearchUserDto;
import com.example.avhedo.dto.userAllDto.UserDto;
import com.example.avhedo.mappers.infoUserMappers.InfoUserMapper;
import com.example.avhedo.mappers.infoUserMappers.InfoUserTwoMapper;
import com.example.avhedo.models.userModels.InfoUser;
import com.example.avhedo.repositories.infoUserRepositories.InfoUserRepository;
import com.example.avhedo.services.MinioComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InfoUserService {
    private final InfoUserRepository infoUserRepository;
    private final InfoUserMapper infoUserMapper;
    private final InfoUserTwoMapper infoUserTwoMapper;
    private final MinioComponent minioComponent;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    @Value("${fileUrl}")
    private String url;
    @Value("${sql.searchNew}")
    private String searchNewSql;
    @Value("${sql.setUrl}")
    private String setUrlSql;
    @Value("${sql.addInfoUser}")
    private String addUserInfo;
    @Value("${sql.updateInfoUser}")
    private String updateUserInfo;


    public InfoUser findByUserInfo(Long id) {
        return infoUserRepository.findByUserInfoId(id).get();
    }

    public InfoUser findById(Long id) {
        return infoUserRepository.findById(id).get();
    }

    public void editUserInfo(Long id, InfoUser infoUser) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(updateUserInfo)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sex",infoUser.getSex());
        params.addValue("hobby",infoUser.getHobby());
        params.addValue("dateOfBirthday",infoUser.getDateOfBirthday());
        params.addValue("aboutMe",infoUser.getAboutMe());
        params.addValue("id",id);
        jdbcTemplate.update(sql,params);
    }


    public void addNewUserInfo(Long id, InfoUser infoUser) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(addUserInfo)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("aboutMe",infoUser.getAboutMe());
        params.addValue("dateOfBirthday",infoUser.getDateOfBirthday());
        params.addValue("hobby",infoUser.getHobby());
        params.addValue("sex",infoUser.getSex());
        params.addValue("id",id);
        jdbcTemplate.update(sql,params);
    }

    public InfoUserDto getUserInfoDto(Long id) {
        return infoUserMapper.toDTO(infoUserRepository.findById(id).get());
    }

    public List<SearchUserDto> searchByName(String name, Long id) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(searchNewSql)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        params.addValue("name","%" + name + "%");
        return jdbcTemplate.query(sql, params , (resultSet, rowNum) -> {
            SearchUserDto searchUserDto = new SearchUserDto();
            UserDto userDto = new UserDto();
            userDto.setId(resultSet.getLong("user_id"));
            userDto.setUsername(resultSet.getString("username"));
            searchUserDto.setUrl(resultSet.getString("url"));
            searchUserDto.setDateOfBirthday(resultSet.getString("date_of_birthday"));
            searchUserDto.setUserDto(userDto);
            return searchUserDto;
        });
    }

    public void uploadFileToMinIO(MultipartFile file, Long id) throws IOException {
        InputStream in = new ByteArrayInputStream(file.getBytes());
        String fileName = file.getOriginalFilename();
        minioComponent.putObject(fileName, in);
        String sql = new String(Files.readAllBytes(Paths.get(setUrlSql)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        params.addValue("url",url + fileName);
        jdbcTemplate.update(sql, params);
    }

    public List<InfoUserFromLentaDto> getAllInfoUserDto(Long id) {
        return infoUserRepository.findAll().stream()
                .filter(userFromLentaDto -> userFromLentaDto.getUserInfo().getId() != id)
                .map(el -> infoUserTwoMapper.toDTO(el))
                .collect(Collectors.toList());
    }

    public InfoUser findByUserInfoByUserId(Long id) {
        return infoUserRepository.findByUserInfoId(id).get();
    }




}
