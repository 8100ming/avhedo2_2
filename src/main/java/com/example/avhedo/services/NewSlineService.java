package com.example.avhedo.services;

import com.example.avhedo.dto.newSline.NewSlineDto;
import com.example.avhedo.mappers.newSlineMappers.NewSlineMappers;
import com.example.avhedo.models.newSlineModels.NewSline;
import com.example.avhedo.repositories.NewSlineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NewSlineService {
    private final NewSlineRepository newSlineRepository;
    private final NewSlineMappers newSlineMappers;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    @Value("${sql.addNewSlineLike}")
    private String addNewSlineLike;
    @Value("${sql.addNewSlineComments}")
    private String addNewSlineComments;
    @Value("${sql.deleteNewSlineLike}")
    private String deleteNewSlineLike;

    public NewSline save(NewSline newSline) {
        return newSlineRepository.save(newSline);
    }

    public void addLikeNewSline(Long userId, Long newSlineId) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(addNewSlineLike)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId",userId);
        params.addValue("newSlineId",newSlineId);
        jdbcTemplate.update(sql,params);
    }

    public void addCommentNewSline(Long userId, Long newSlineId, String content) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(addNewSlineComments)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("text",content);
        params.addValue("userId",userId);
        params.addValue("newSlineId",newSlineId);
        jdbcTemplate.update(sql,params);
    }

    public void deleteLikeNewSline(Long userId, Long newSlineId) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(deleteNewSlineLike)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId",userId);
        params.addValue("newSlineId",newSlineId);
        jdbcTemplate.update(sql, params);
    }

    public NewSlineDto findByIdDto(Long newSlineId) {
        return newSlineMappers.toDTO(newSlineRepository.findById(newSlineId).get());
    }

    public List<NewSlineDto> findAllnewSlineDto(PageRequest pageRequest) {
        return newSlineRepository.findAll(pageRequest).get().map(el -> newSlineMappers.toDTO(el)).collect(Collectors.toList());
    }

}
