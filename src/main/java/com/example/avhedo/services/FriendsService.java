package com.example.avhedo.services;

import com.example.avhedo.dto.FriendsDto;
import com.example.avhedo.mappers.friendsMappers.FriendsMapper;
import com.example.avhedo.repositories.FriendsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FriendsService {
    private final FriendsRepository friendsRepository;
    private final FriendsMapper friendsMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    @Value("${sql.deleteFriends}")
    private String deleteFriends;
    @Value("${sql.addFriends}")
    private String addFriends;

    public void addNewFriends(Long id, Long idTwo) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(addFriends)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("ownerId",id);
        params.addValue("friendsId",idTwo);
        jdbcTemplate.update(sql, params);
    }

    public void deleteFrineds(Long id, Long idTwo) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(deleteFriends)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("ownerId",id);
        params.addValue("friendsId",idTwo);
        jdbcTemplate.update(sql, params);
    }

    public List<FriendsDto> findAllFriends(Long id) {
        return friendsRepository.findAllByPageOwnersId(id).stream()
                .map(el -> friendsMapper.toDTO(el)).collect(Collectors.toList());
    }

    public List<FriendsDto> findAllSubFriends(Long id) {
        return friendsRepository.findAllByFriendsId(id).stream()
                .map(el -> friendsMapper.toDTO(el)).collect(Collectors.toList());
    }

}
