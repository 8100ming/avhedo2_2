package com.example.avhedo.services.kafka;
import com.example.avhedo.dto.authorizeDto.SignupRequest;
import com.example.avhedo.services.userServices.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component
public class UserConsumer {

    @Autowired
    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(UserConsumer.class);
    private final ConcurrentLinkedQueue<String> messageQueue = new ConcurrentLinkedQueue<>();

    @KafkaListener(topics = "user-topic", groupId = "group-id", containerFactory = "kafkaListenerContainerFactory")
    public void consumeUser(String jsonSignupRequests) {
        messageQueue.offer(jsonSignupRequests);
        processMessages();
    }

    public void processMessages() {
        while (!messageQueue.isEmpty()) {
            String jsonSignupRequests = messageQueue.poll();
            List<SignupRequest> signupRequests = userService.convertJsonToList(jsonSignupRequests);
            signupRequests.stream().forEach(signupRequest -> {
                try {
                    userService.saveUser(signupRequest);
                    logger.info("Пользователь добавлен: {}", signupRequest.getUsername());
                } catch (Exception e) {
                    logger.error("Произошла ошибка при обработке пользователя. Exception: {}. JSON Request: {}", e, signupRequest);
                }
            });
        }
    }
}

