package com.example.avhedo.services.kafka;

import com.example.avhedo.dto.authorizeDto.SignupRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserProducer {

    @Autowired
    private KafkaTemplate<String, SignupRequest> kafkaTemplate;

}