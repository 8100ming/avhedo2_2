package com.example.avhedo.services;

import com.example.avhedo.dto.infoAllUserDto.InfoUserDto;
import com.example.avhedo.dto.userAllDto.UserDto;
import com.example.avhedo.models.OnlineDatingPeople;
import com.example.avhedo.repositories.OnlineDatingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OnlineDatingService {
    private final OnlineDatingRepository onlineDatingRepository;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    @Value("${sql.getInfoAllPeopleWithOnlineDating}")
    private String getInfoAllPeopleWithOnlineDatin;
    @Value("${sql.getAllLikedUsers}")
    private String getAllLikedUsers;
    @Value("${sql.likePeople}")
    private String likePeople;
    @Value("${sql.onlineDatingDelete}")
    private String onlineDatingDelete;

    public OnlineDatingPeople save(OnlineDatingPeople onlineDatingPeople) {
        return onlineDatingRepository.save(onlineDatingPeople);
    }

    public List<InfoUserDto> allPeopleWithOnlineDating(Long id, Integer offset, Integer limit) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(getInfoAllPeopleWithOnlineDatin)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId",id);
        params.addValue("idWhoLove",id);
        params.addValue("limit",limit);
        params.addValue("offset",offset);
        return jdbcTemplate.query(sql, params, (resultSet, rowNum) -> {
            InfoUserDto infoUserDto = new InfoUserDto();
            infoUserDto.setDateOfBirthday(resultSet.getString("date_of_birthday"));
            infoUserDto.setAboutMe(resultSet.getString("about_me"));
            infoUserDto.setHobby(resultSet.getString("hobby"));
            infoUserDto.setUrl(resultSet.getString("url"));
            infoUserDto.setSex(resultSet.getString("sex"));
            UserDto userDto = new UserDto();
            userDto.setId(resultSet.getLong("user_id"));
            userDto.setUsername(resultSet.getString("username"));
            infoUserDto.setUserDto(userDto);
            return infoUserDto;
        });
    }

    public List<InfoUserDto> getInfoAllPeopleLiked(Long id) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(getAllLikedUsers)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        return jdbcTemplate.query(sql, params, (resultSet, rowNum) -> {
            InfoUserDto infoUserDto = new InfoUserDto();
            infoUserDto.setDateOfBirthday(resultSet.getString("date_of_birthday"));
            infoUserDto.setAboutMe(resultSet.getString("about_me"));
            infoUserDto.setHobby(resultSet.getString("hobby"));
            infoUserDto.setUrl(resultSet.getString("url"));
            infoUserDto.setSex(resultSet.getString("sex"));
            UserDto userDto = new UserDto();
            userDto.setId(resultSet.getLong("id"));
            userDto.setUsername(resultSet.getString("username"));
            infoUserDto.setUserDto(userDto);
            return infoUserDto;
        });
    }

    public void LikePeople(Long id, Long idTwo) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(likePeople)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idWhoLove",id);
        params.addValue("idWhichLove",idTwo);
        jdbcTemplate.update(sql,params);
    }

    public void deleteOnlineDating(Long id, Long idTwo) throws IOException {
        String sql = new String(Files.readAllBytes(Paths.get(onlineDatingDelete)));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idWhoLove",id);
        params.addValue("idWhichLove",idTwo);
        jdbcTemplate.update(sql, params);
    }

}

