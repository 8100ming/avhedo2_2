package com.example.avhedo.dto;

import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import lombok.Data;

@Data
public class FriendsDto {
    //private InfoUserFromLentaDto ownerPage;
    private InfoUserFromLentaDto friends;
}
