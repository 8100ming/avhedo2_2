package com.example.avhedo.dto.newSline;

import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LikesNewSlineDto {
    private Long id;
    private InfoUserFromLentaDto userWhichLikes;

}
