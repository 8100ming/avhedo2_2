package com.example.avhedo.dto.newSline;

import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewSlineDto {
    private Long id;
    private String textMessage;
    private String titleMessage;
    private InfoUserFromLentaDto userNewSline;
    private List<CommentsDto> commentsList;
    private List<LikesNewSlineDto> likesNewSlines;
}
