package com.example.avhedo.dto.userAllDto;

import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String username;
}