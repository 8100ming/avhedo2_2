package com.example.avhedo.dto.userAllDto;

import com.example.avhedo.services.userServices.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailsDto {
    public String jwt;
    public UserDetailsImpl userDetails;
    public List<String> roles;
}
