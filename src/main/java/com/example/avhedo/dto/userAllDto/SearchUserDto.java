package com.example.avhedo.dto.userAllDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchUserDto {
    private UserDto userDto;
    private String dateOfBirthday;
    private String url;
}
