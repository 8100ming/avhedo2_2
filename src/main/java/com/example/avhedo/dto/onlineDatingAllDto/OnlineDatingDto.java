package com.example.avhedo.dto.onlineDatingAllDto;

import com.example.avhedo.dto.userAllDto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OnlineDatingDto {
    //User
    private UserDto userDto;
    //UserInfo
    private String aboutMe;
    private String hobby;
    private String url;
    private String dateOfBirthday;
    private Boolean mutualLike;

}
