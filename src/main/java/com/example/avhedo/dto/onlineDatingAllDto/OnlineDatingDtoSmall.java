package com.example.avhedo.dto.onlineDatingAllDto;

import lombok.Data;

@Data
public class OnlineDatingDtoSmall {
    private Long idPeopleWichLove;
}
