package com.example.avhedo.dto.infoAllUserDto;

import com.example.avhedo.dto.userAllDto.UserDto;
import lombok.Data;

@Data
public class InfoUserFromLentaDto {
    private UserDto userInfo;
    private String dateOfBirthday;
    private String url;
}
