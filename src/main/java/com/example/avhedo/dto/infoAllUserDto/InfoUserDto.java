package com.example.avhedo.dto.infoAllUserDto;

import com.example.avhedo.dto.userAllDto.UserDto;
import lombok.Data;

@Data
public class InfoUserDto {
    private String dateOfBirthday;
    private String aboutMe;
    private String hobby;
    private String url;
    private String sex;
    private UserDto userDto;
}
