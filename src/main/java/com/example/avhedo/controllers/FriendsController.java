package com.example.avhedo.controllers;

import com.example.avhedo.dto.FriendsDto;
import com.example.avhedo.services.FriendsService;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping()
@RequiredArgsConstructor
public class FriendsController {
    @Autowired
    private final FriendsService friendsService;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FriendsController.class);
    @PostMapping("addFriends/{ownerId}/{friendId}")
    public void addFriends(@PathVariable("ownerId") Long ownerId, @PathVariable("friendId") Long friendsId) throws IOException {
        friendsService.addNewFriends(ownerId, friendsId);
        logger.info("Пользователи добавили друзей  id = {} , idTwo = {}",friendsId,ownerId);
    }

    @DeleteMapping("deleteFriends/{ownerId}/{friendId}")
    public void deleteFriends(@PathVariable("ownerId") Long ownerId, @PathVariable("friendId") Long friendsId) throws IOException {
        friendsService.deleteFrineds(ownerId, friendsId);
        logger.info("Пользователь удален из друзей  id = {}",friendsId);
    }

    @GetMapping("GetAllFriendsUser/{ownerid}")
    public List<FriendsDto> allFriends(@PathVariable("ownerid") Long ownerId) {
        return friendsService.findAllFriends(ownerId);
    }

    @GetMapping("GetAllSubFriendsUser/{ownerid}")
    public List<FriendsDto> allSubFriends(@PathVariable("ownerid") Long ownerId) {
        return friendsService.findAllSubFriends(ownerId);
    }
}
