package com.example.avhedo.controllers;


import com.example.avhedo.dto.newSline.NewSlineDto;
import com.example.avhedo.models.newSlineModels.NewSline;
import com.example.avhedo.services.NewSlineService;
import com.example.avhedo.services.infoUserServices.InfoUserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping()
@RequiredArgsConstructor
public class NewSlineController {
    @Autowired
    private InfoUserService infoUserService;
    @Autowired
    private NewSlineService newSlineService;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(NewSlineController.class);

    @PostMapping("/userAddNewSline/{userId}")
    public NewSline addNewSline(@PathVariable("userId") Long userId,
                                @RequestBody(required = false) NewSline newSline
    ) {
        newSline.setUserNewSline(infoUserService.findByUserInfoByUserId(userId));
        return newSlineService.save(newSline);
    }

    @PostMapping(value = "userAddLikeNewSline/{userId}/{newSlineId}")
    public void userAddLike(@PathVariable("userId") Long userId,
                            @PathVariable("newSlineId") Long newSlineId
    ) throws  IOException {
        newSlineService.addLikeNewSline(userId, newSlineId);
        logger.info("Пользователи добавили лайк  id = {} , idNewSline = {}",userId,newSlineId);
    }

    @PostMapping(value = "userAddCommentNewSline/{userId}/{newSlineId}")
    public void addComments(@PathVariable("userId") Long userId,
                            @PathVariable("newSlineId") Long newSlineId,
                            @RequestParam("content") String content
    ) throws  IOException {
        newSlineService.addCommentNewSline(userId, newSlineId, content);
        logger.info("Пользователи добавили коммент  id = {} , idNewSline = {}",userId,newSlineId);
    }

    @DeleteMapping(value = "userDeleteLikeNewSline/{userId}/{newSlineId}")
    public void deleteLikes(@PathVariable("userId") Long userId, @PathVariable("newSlineId") Long newSlineId) throws IOException {
        newSlineService.deleteLikeNewSline(userId, newSlineId);
        logger.info("Пользователи удалил лайк  id = {} , idNewSline = {}",userId,newSlineId);
    }

    @GetMapping(value = "getNewSline/{newSlineId}")
    public NewSlineDto getLentaMessage(@PathVariable("newSlineId") Long newSlineId) {
        return newSlineService.findByIdDto(newSlineId);
    }

    @GetMapping(value = "getAllNewSline")
    public List<NewSlineDto> getInfoMessage(@RequestParam("offset") Integer offset,
                                            @RequestParam(value = "limit", defaultValue = "3") Integer limit) {
        return newSlineService.findAllnewSlineDto(PageRequest.of(limit, offset));
    }
}
