package com.example.avhedo.controllers;

import com.example.avhedo.dto.ChatMessagesDTO;
import com.example.avhedo.models.chatModels.ChatMessage;
import com.example.avhedo.models.chatModels.ChatNotification;
import com.example.avhedo.models.exception.ChatNotFoundException;
import com.example.avhedo.models.exception.MessageNotFoundException;
import com.example.avhedo.services.chatMessageServices.ChatMessageService;
import com.example.avhedo.services.chatMessageServices.ChatNotificationService;
import com.example.avhedo.services.chatMessageServices.ChatRoomService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Controller
@RestController
@RequiredArgsConstructor
public class ChatController {
    @Autowired
    private ChatRoomService chatRoomService;
    @Autowired
    private ChatMessageService chatMessageService;
    @Autowired
    private ChatNotificationService chatNotificationService;
    private static final Logger logger = LoggerFactory.getLogger(ChatController.class.getName());

    @MessageMapping("/chat")
    public void processMessage(@Payload ChatMessage chatMessage) throws IOException {
        chatRoomService.processMessages(chatMessage);
        logger.info("Сообщение отправленно = {} ",chatMessage);
    }


    @GetMapping("GetMessages/{senderId}/{recipientId}")
    public List<ChatMessage> findChatMessages(@PathVariable String senderId,
                                              @PathVariable String recipientId) throws ChatNotFoundException {
        return chatMessageService.findAllBySenderIdAndAndRecipientId(senderId, recipientId);
    }

    @GetMapping("GetChatMessages/{senderId}/{recipientId}")
    public List<ChatMessage> findAllMessages(@PathVariable String senderId,
                                             @PathVariable String recipientId) throws ChatNotFoundException, IOException {
        return chatMessageService.findAllByChatId(senderId, recipientId);
    }

    @GetMapping("GetAllChatUser/{userId}")
    public List<ChatMessagesDTO> findAllChatUser(@PathVariable Long userId) throws IOException{
        return chatMessageService.getAllChatUser(userId);
    }

    @GetMapping("GetMessageChatForId/{Id}")
    public ChatMessage findChatForId(@PathVariable Long Id) {
        return chatMessageService.findById(Id).orElseThrow(() ->
                new MessageNotFoundException("Сообщение с Id " + Id + " не найдено"));
    }

    @GetMapping("GetNotificationMessages/{recipientId}")
    public List<ChatNotification> findAllNotificationsMessages(@PathVariable String recipientId) {
        return chatNotificationService.findAllByRecipientId(recipientId);
    }

    @DeleteMapping("deleteNotificationMessages/{recipientId}/{senderId}")
    public void deleteNotificationsMessages(@PathVariable String recipientId, @PathVariable String senderId) throws IOException {
        chatNotificationService.deleteNotification(recipientId, senderId);
        logger.info("Уведомления удалены recipientId {}, senderId {}",recipientId,senderId);
    }

}
