package com.example.avhedo.controllers;

import com.example.avhedo.dto.infoAllUserDto.InfoUserDto;
import com.example.avhedo.models.OnlineDatingPeople;
import com.example.avhedo.services.OnlineDatingService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping()
@RequiredArgsConstructor
public class OnlineDatingController {
    @Autowired
    private OnlineDatingService onlineDatingService;
    private static final Logger logger = LoggerFactory.getLogger(OnlineDatingController.class.getName());

    @GetMapping(value = "GetInfoAllPeopleLove/{id}")
    public List<InfoUserDto> allLovePeople(@PathVariable Long id,
                                           @RequestParam("offset") Integer offset,
                                           @RequestParam("limit") Integer limit) throws IOException {
        return onlineDatingService.allPeopleWithOnlineDating(id, offset, limit);
    }

    @PostMapping(value = "people/{id}/love/{idTwo}")
    public void likesPeople(@PathVariable Long id, @PathVariable Long idTwo) {
        onlineDatingService.save(new OnlineDatingPeople(id, idTwo, false));
        logger.info("Пользователь лайкнут с id = {}",idTwo);
    }

    @PostMapping(value = "{id}/loveTo/{idTwo}")
    public void likesPeopleMutual(@PathVariable Long id, @PathVariable Long idTwo) throws IOException {
        onlineDatingService.LikePeople(id, idTwo);
        logger.info("Пользователь лайнкнут c id = {}",idTwo);
    }


    @DeleteMapping(value = "people/{id}/dontLove/{idTwo}")
    public void deleteLikesPeople(@PathVariable Long id, @PathVariable Long idTwo) throws IOException {
        onlineDatingService.deleteOnlineDating(id, idTwo);
        logger.info("Пользователь не нравится с id = {}",idTwo);
    }

    @GetMapping(value = "getAllLiked/{id}")
    public List<InfoUserDto> infoAllLove(@PathVariable Long id) throws IOException {
        return onlineDatingService.getInfoAllPeopleLiked(id);
    }
}


