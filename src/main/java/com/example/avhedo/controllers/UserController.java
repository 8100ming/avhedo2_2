package com.example.avhedo.controllers;

import com.example.avhedo.dto.infoAllUserDto.InfoUserDto;
import com.example.avhedo.dto.infoAllUserDto.InfoUserFromLentaDto;
import com.example.avhedo.dto.userAllDto.SearchUserDto;
import com.example.avhedo.models.userModels.InfoUser;
import com.example.avhedo.services.infoUserServices.InfoUserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping()
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private InfoUserService infoUserService;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    @PostMapping(value = "userAddInfo/{userid}")
    public void addInfo(@PathVariable("userid") Long id, @RequestBody(required = false) InfoUser infoUser) throws IOException {
        infoUserService.addNewUserInfo(id, infoUser);
        logger.info("Пользователь добавил личную информацию с id = {}",id);
    }

    @PostMapping("/upload/{userid}")
    @Transactional
    public void uploadFileToMinIO(@PathVariable("userid") Long id, @RequestParam("file") MultipartFile file) throws IOException {
        infoUserService.uploadFileToMinIO(file, id);
        logger.info("Файл загружен у пользователя с id = {}",id);
    }



    @GetMapping("/GetUserInfo/{userid}")
    public InfoUserDto InfoToUser(@PathVariable("userid") Long id) {
        return infoUserService.getUserInfoDto(id);
    }


    @GetMapping("/GetAlluserInfo/{userid}")
    public List<InfoUserFromLentaDto> AllInfoToUser(@PathVariable("userid") Long id) {
        return infoUserService.getAllInfoUserDto(id);
    }

    @PostMapping("/editUserInfo/{userid}")
    public void editUserInfo(@PathVariable("userid") Long id, @RequestBody(required = false) InfoUser infoUser) throws IOException {
        infoUserService.editUserInfo(id, infoUser);
        logger.info("Пользователь изменил личную информацию с id = {}",id);
    }

    @GetMapping("/searchUser/{id}")
    public List<SearchUserDto> searchByName(@RequestParam("name") String name, @PathVariable("id") Long idOwl) throws IOException {
        return infoUserService.searchByName(name, idOwl);
    }

    @PostMapping("/deleteUrl/{id}")
    public void deleteUrl(@PathVariable("id") Long id) {
        infoUserService.findById(id).setUrl("");
        logger.info("Файл удален у пользователя с id = {}",id);
    }
}

