package com.example.avhedo.controllers;

import com.example.avhedo.dto.authorizeDto.LoginRequest;
import com.example.avhedo.dto.authorizeDto.SignupRequest;
import com.example.avhedo.dto.userAllDto.UserDetailsDto;
import com.example.avhedo.models.userModels.User;
import com.example.avhedo.services.userServices.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/signin")
    public UserDetailsDto authUser(@RequestBody LoginRequest loginRequest) {
        return userService.signInUser(loginRequest);
    }

    @PostMapping("/signup")
    public User registerUser(@RequestBody SignupRequest signupRequest) {
        return userService.saveUser(signupRequest);
    }
}
