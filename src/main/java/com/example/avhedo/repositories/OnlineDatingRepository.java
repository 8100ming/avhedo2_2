package com.example.avhedo.repositories;

import com.example.avhedo.models.OnlineDatingPeople;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OnlineDatingRepository extends JpaRepository<OnlineDatingPeople, Long> {
    List<OnlineDatingPeople> findAllByIdPeopleWichLove(Long id);

    List<OnlineDatingPeople> findAllByIdPeopleWhoLoveAndIdPeopleWichLove(Long idPeopleWhoLove, Long idPeopleWichLove);

}
