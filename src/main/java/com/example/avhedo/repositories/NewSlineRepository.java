package com.example.avhedo.repositories;

import com.example.avhedo.models.newSlineModels.NewSline;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NewSlineRepository extends JpaRepository<NewSline, Long> {
    Optional<NewSline> findById(Long id);
}
