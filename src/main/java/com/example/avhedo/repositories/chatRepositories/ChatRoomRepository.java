package com.example.avhedo.repositories.chatRepositories;

import com.example.avhedo.models.chatModels.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long> {
    Optional<ChatRoom> findAllByRecipientIdAndSenderId(String recipientId, String senderId);

}
