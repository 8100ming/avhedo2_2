package com.example.avhedo.repositories.chatRepositories;

import com.example.avhedo.models.chatModels.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {
    List<ChatMessage> findAllBySenderIdAndAndRecipientId(String senderId, String recipientId);

    Optional<ChatMessage> findById(Long id);
}
