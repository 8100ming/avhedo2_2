package com.example.avhedo.repositories.chatRepositories;


import com.example.avhedo.models.chatModels.ChatNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatNotificationRepository extends JpaRepository<ChatNotification, Long> {
    List<ChatNotification> findAllByRecipientId(String recipientId);
}
