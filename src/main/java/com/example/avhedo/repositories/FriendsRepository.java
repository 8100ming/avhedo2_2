package com.example.avhedo.repositories;

import com.example.avhedo.models.Friends;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendsRepository extends JpaRepository<Friends, Long> {
    List<Friends> findAllByPageOwnersId(Long ownerId);

    List<Friends> findAllByFriendsId(Long id);

}
