package com.example.avhedo.repositories.infoUserRepositories;

import com.example.avhedo.models.userModels.InfoUser;
import com.example.avhedo.models.userModels.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InfoUserRepositoryTwo extends JpaRepository<InfoUser, Long> {
    List<InfoUser> findByUserInfo(User user);
}
