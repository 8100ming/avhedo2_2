package com.example.avhedo.repositories.infoUserRepositories;

import com.example.avhedo.models.userModels.InfoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InfoUserRepository extends JpaRepository<InfoUser, Long> {
    Optional<InfoUser> findByUserInfoId(Long id);

    Optional<InfoUser> findById(Long id);

    List<InfoUser> findAll();

}
