package com.example.avhedo.repositories.userRepositories;

import com.example.avhedo.models.userModels.ERole;
import com.example.avhedo.models.userModels.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
