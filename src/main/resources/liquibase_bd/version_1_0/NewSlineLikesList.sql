create table new_sline_likes_new_slines
(
    new_sline_id        bigint not null
        references new_sline,
    likes_new_slines_id bigint not null
        unique
        references likes_new_sline
);