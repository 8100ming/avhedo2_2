create table comments
(
    id            bigserial
        primary key,
    text_comments varchar(255),
    user_id       bigint
        references info_user
);