create table users
(
    id       bigserial
        primary key,
    email    varchar(255)
        unique,
    password varchar(255),
    username varchar(255)
        unique
);