create table new_sline_comments_list
(
    new_sline_id     bigint not null
        constraint fk5kavqkvk5jhveuai2gs9b48mp
            references new_sline,
    comments_list_id bigint not null
        unique
        references comments
);