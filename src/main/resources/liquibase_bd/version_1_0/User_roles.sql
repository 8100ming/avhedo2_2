create table user_roles
(
    user_id bigint not null
        references users,
    role_id bigint not null
        references roles,
    primary key (user_id, role_id)
);