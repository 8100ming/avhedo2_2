create table friends
(
    id         bigserial
        primary key,
    friends_id bigint
        references info_user,
    owner_id   bigint
        references info_user
);