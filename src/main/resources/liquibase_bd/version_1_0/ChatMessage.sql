create table chat_message
(
    id             bigserial
        primary key,
    chat_id        varchar(255),
    content        varchar(255),
    recipient_id   varchar(255),
    sender_id      varchar(255),
    recipient_name varchar(255),
    sender_name    varchar(255),
    status         integer,
    timestamp      timestamp
);
