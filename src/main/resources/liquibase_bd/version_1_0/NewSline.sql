create table new_sline
(
    id            bigserial
        primary key,
    text_message  varchar(255),
    title_message varchar(255),
    user_id       bigint
        references info_user
);
