create table chat_notification
(
    id           bigserial primary key,
    sender_id    varchar(255),
    sender_name  varchar(255),
    recipient_id varchar(255)
);