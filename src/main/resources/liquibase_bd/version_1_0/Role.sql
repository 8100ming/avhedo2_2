create table roles
(
    id   bigserial
        primary key,
    name varchar(20)
);
INSERT INTO roles (name)
SELECT 'ROLE_USER'
WHERE NOT EXISTS (SELECT * FROM roles WHERE name = 'ROLE_USER');
INSERT INTO roles (name)
SELECT 'ROLE_MODERATOR'
WHERE NOT EXISTS (SELECT * FROM roles WHERE name = 'ROLE_MODERATOR');
INSERT INTO roles (name)
SELECT 'ROLE_ADMIN'
WHERE NOT EXISTS (SELECT * FROM roles WHERE name = 'ROLE_ADMIN');