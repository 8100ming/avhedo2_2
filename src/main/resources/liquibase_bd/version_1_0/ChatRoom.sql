create table chat_room
(
    id           serial primary key,
    chat_id      varchar(255),
    recipient_id varchar(255),
    sender_id    varchar(255)
);