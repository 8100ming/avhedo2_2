create table likes_new_sline
(
    id            bigserial
        primary key,
    likes_message boolean,
    user_id       bigint
        references info_user
);