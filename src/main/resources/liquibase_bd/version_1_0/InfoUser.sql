create table info_user
(
    id               SERIAL PRIMARY KEY,
    about_me         varchar(255),
    date_of_birthday varchar(255),
    hobby            varchar(255),
    url              varchar(255),
    user_id          bigint
        REFERENCES users,
    sex              varchar(255)
);