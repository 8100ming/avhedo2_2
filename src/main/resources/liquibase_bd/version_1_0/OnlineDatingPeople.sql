create table online_dating_people
(
    id                  bigserial
        primary key,
    id_people_who_love  bigint,
    id_people_wich_love bigint,
    mutual_like         boolean
);