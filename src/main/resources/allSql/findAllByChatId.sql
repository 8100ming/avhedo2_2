SELECT cm.*
FROM chat_message cm
         JOIN chat_room cr ON cm.chat_id = cr.chat_id
WHERE (cr.sender_id = :senderId AND cr.recipient_id = :recipientId)