SELECT *
FROM info_user iu
         JOIN Users u ON iu.user_id = u.id
WHERE u.username LIKE :name
  AND iu.id != :id