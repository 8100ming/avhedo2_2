WITH RankedMessages AS (SELECT cm.id,
                               cr.chat_id,
                               cr.sender_id                                                           AS recipientId,
                               cr.recipient_id                                                        AS senderId,
                               cm.sender_id                                                           AS lastSenderId,
                               cm.content,
                               cm.timestamp,
                               ROW_NUMBER() OVER (PARTITION BY cr.chat_id ORDER BY cm.timestamp DESC) AS row_num
                        FROM chat_message cm
                                 JOIN
                             chat_room cr ON cm.chat_id = cr.chat_id
                        WHERE cr.recipient_id = :userId)
SELECT rm.id,
       rm.chat_id  AS chatId,
       rm.senderId,
       rm.recipientId,
       rm.content,
       rm.lastSenderId,
       ur.username AS recipientName,
       us.username AS senderName,
       iu.url      AS urlSender,
       rm.timestamp
FROM RankedMessages rm
         JOIN
     info_user iu ON rm.recipientId = iu.user_id::text
         JOIN
     users us ON rm.senderId = us.id::text
         JOIN
     users ur ON rm.recipientId = ur.id::text
WHERE rm.row_num = 1;