SELECT IU.*, US.*
FROM info_user IU
         JOIN users US ON IU.user_id = US.id
WHERE IU.sex != (SELECT sex FROM info_user WHERE user_id = :userId)
  AND NOT EXISTS (SELECT 1
                  FROM online_dating_people odp
                  WHERE odp.id_people_who_love = :idWhoLove
                    AND odp.id_people_wich_love = IU.user_id)
LIMIT :limit OFFSET :offset;