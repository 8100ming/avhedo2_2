INSERT INTO info_user (about_me, date_of_birthday, hobby, user_id, sex)
SELECT :aboutMe, :dateOfBirthday, :hobby, id, :sex
FROM users
WHERE id = :id;