UPDATE info_user
SET sex              = :sex,
    hobby            = :hobby,
    date_of_birthday = :dateOfBirthday,
    about_me         = :aboutMe
WHERE id = :id;