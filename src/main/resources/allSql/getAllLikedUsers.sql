WITH LoveInfo AS (SELECT US.id,
                         US.username,
                         ODP.mutual_like,
                         IU.about_me,
                         IU,
                         sex,
                         IU.date_of_birthday,
                         IU.hobby,
                         IU.url
                  FROM online_dating_people ODP
                           INNER JOIN
                       info_user IU ON ODP.id_people_who_love = IU.user_id
                           INNER JOIN
                       users US ON US.id = IU.user_id
                  WHERE ODP.id_people_wich_love = :id)
SELECT *
FROM LoveInfo;