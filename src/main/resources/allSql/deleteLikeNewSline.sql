WITH deleted_likes AS (
    DELETE FROM new_sline_likes_new_slines
        WHERE new_sline_id = :newSlineId
            AND likes_new_slines_id IN (SELECT id
                                        FROM likes_new_sline
                                        WHERE likes_message = true
                                          AND user_id = :userId)
        RETURNING likes_new_slines_id)
DELETE
FROM likes_new_sline
WHERE id IN (SELECT likes_new_slines_id FROM deleted_likes);