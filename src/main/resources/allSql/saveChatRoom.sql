INSERT INTO chat_room (chat_id, sender_id, recipient_id)
VALUES (:chatId, :senderId, :recipientId),
       (:chatId, :recipientId, :senderId)