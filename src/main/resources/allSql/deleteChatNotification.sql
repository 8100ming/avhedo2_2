DELETE
FROM chat_notification
WHERE sender_id = :senderId
  AND recipient_id = :recipientId;
