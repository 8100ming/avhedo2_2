WITH source AS (UPDATE online_dating_people
    SET mutual_like = true
    WHERE id_people_who_love = :idWhoLove
        AND id_people_wich_love = :idWhichLove)
INSERT
INTO online_dating_people (id_people_who_love, id_people_wich_love, mutual_like)
VALUES (:idWhichLove, :idWhoLove, true)