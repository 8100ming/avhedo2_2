package com.example.avhedo;


import com.example.avhedo.controllers.FriendsController;
import com.example.avhedo.controllers.OnlineDatingController;
import com.example.avhedo.dto.FriendsDto;
import com.example.avhedo.mappers.friendsMappers.FriendsMapper;
import com.example.avhedo.mappers.friendsMappers.FriendsMapperTwo;
import com.example.avhedo.models.Friends;
import com.example.avhedo.models.OnlineDatingPeople;
import com.example.avhedo.models.userModels.InfoUser;
import com.example.avhedo.models.userModels.User;
import com.example.avhedo.repositories.FriendsRepository;
import com.example.avhedo.repositories.OnlineDatingRepository;
import com.example.avhedo.repositories.infoUserRepositories.InfoUserRepositoryTwo;
import com.example.avhedo.services.FriendsService;
import com.example.avhedo.services.infoUserServices.InfoUserService;
import com.example.avhedo.services.userServices.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class DemoauthApplicationTests {
    @Autowired
    private OnlineDatingController peopleController;
    @Autowired
    private FriendsController friendsController;
    @MockBean
    private OnlineDatingRepository onlineDatingRepository;
    @MockBean
    private UserService userService;
    @MockBean
    private InfoUserRepositoryTwo infoUserRepositoryTwo;
    @MockBean
    private InfoUserService infoUserService;
    @MockBean
    private FriendsRepository friendsRepository;
    @MockBean
    private FriendsService friendsService;
    @MockBean
    private FriendsMapper friendsMapper;
    @MockBean
    private FriendsMapperTwo friendsMapperTwo;

    @Test
    public void testLikesPeople() {
        Long id1 = 1L;
        Long id2 = 2L;
        when(onlineDatingRepository.save(any())).thenReturn(new OnlineDatingPeople(id1, id2, false));
        //ResponseEntity<MessageResponse> response = peopleController.likesPeople(id1, id2);
        // assertEquals("Запрос отправлен", response.getBody().getMessage());
    }

    @Test
    public void testLikesPeopleMutual() throws IOException, SQLException {
        Long id1 = 1L;
        Long id2 = 2L;
        OnlineDatingPeople onlineDatingPeopleOne = new OnlineDatingPeople(id1, id2, false);
        OnlineDatingPeople onlineDatingPeopleTwo = new OnlineDatingPeople(id2, id1, true);
        when(onlineDatingRepository.findAllByIdPeopleWhoLoveAndIdPeopleWichLove(id1, id2)).thenReturn(Arrays.asList(onlineDatingPeopleOne));
        when(onlineDatingRepository.save(onlineDatingPeopleOne)).thenReturn(onlineDatingPeopleOne);
        when(onlineDatingRepository.save(onlineDatingPeopleTwo)).thenReturn(onlineDatingPeopleTwo);
        //ResponseEntity<MessageResponse> response = peopleController.likesPeopleMutual(id1, id2);
        //assertEquals("Запрос отправлен", response.getBody().getMessage());
        //assertTrue(onlineDatingPeopleOne.getMutualLike());
    }

    @Test
    public void testDeleteLikesPeople() throws SQLException, IOException {
        Long id1 = 1L;
        Long id2 = 2L;
        OnlineDatingPeople onlineDatingPeopleOne = new OnlineDatingPeople(id1, id2, false);
        when(onlineDatingRepository.findAllByIdPeopleWhoLoveAndIdPeopleWichLove(id2, id1)).thenReturn(Arrays.asList(onlineDatingPeopleOne));
        doNothing().when(onlineDatingRepository).delete(onlineDatingPeopleOne);
        peopleController.deleteLikesPeople(id1, id2);
        verify(onlineDatingRepository, times(1)).delete(onlineDatingPeopleOne);
    }

    @Test
    public void testInfoAllLove() {
        User user = new User(1L, "Danila", "321@gmail.com", "2002");
        User user2 = new User(2L, "Danila2", "321@gmail.com2", "2002");
        InfoUser userInfo = new InfoUser(1L, "22.02.22", "username", "aboutMe", "123", "men", user);
        OnlineDatingPeople onlineDatingPeopleOne = new OnlineDatingPeople(2L, 1L, false);
        InfoUser infoUser = new InfoUser(2L, "22.02.22", "username", "aboutMe", "123", "men", user2);
        when(onlineDatingRepository.findAllByIdPeopleWichLove(1L)).thenReturn(Arrays.asList(onlineDatingPeopleOne));
        when(userService.findById(onlineDatingPeopleOne.getIdPeopleWhoLove())).thenReturn(Optional.of(user));
        when(infoUserRepositoryTwo.findByUserInfo(user)).thenReturn(Arrays.asList(infoUser));
    }

    @Test
    public void testAddFriends() {
        User userOwner = new User(1L, "Danila", "321@gmail.com", "2002");
        User userFriend = new User(2L, "Danila2", "321@gmail.com2", "2002");
        InfoUser owner = new InfoUser(1L, "22.02.22", "username", "aboutMe", "123", "men", userOwner);
        InfoUser friend = new InfoUser(2L, "22.02.22", "username", "aboutMe", "123", "men", userFriend);
        when(infoUserService.findByUserInfo(userOwner.getId())).thenReturn(owner);
        when(infoUserService.findByUserInfo(userFriend.getId())).thenReturn(friend);
        when(userService.findById(owner.getUserInfo().getId())).thenReturn(Optional.of(userOwner));
        when(userService.findById(friend.getUserInfo().getId())).thenReturn(Optional.of(userFriend));
        when(friendsRepository.save(any())).thenReturn(new Friends(owner, friend));
        //ResponseEntity<MessageResponse> response = friendsController.addFriends(userOwner.getId(), userFriend.getId());
        //assertEquals("Вы подписались", response.getBody().getMessage());
    }

    @Test
    public void testDeleteFriends() {
        User userOwner = new User(1L, "Danila", "321@gmail.com", "2002");
        User userFriend = new User(2L, "Danila2", "321@gmail.com2", "2002");
        InfoUser owner = new InfoUser(1L, "22.02.22", "username", "aboutMe", "123", "men", userOwner);
        InfoUser friend = new InfoUser(2L, "22.02.22", "username", "aboutMe", "123", "men", userFriend);
        Friends friends = new Friends(owner, friend);
        when(userService.findById(owner.getUserInfo().getId())).thenReturn(Optional.of(userOwner));
        when(userService.findById(friend.getUserInfo().getId())).thenReturn(Optional.of(userFriend));
        when(infoUserService.findByUserInfo(userOwner.getId())).thenReturn(owner);
        when(infoUserService.findByUserInfo(userFriend.getId())).thenReturn(friend);
        //ResponseEntity<MessageResponse> response = friendsController.deleteFriends(userOwner.getId(), userFriend.getId());
        //assertEquals("Пользователь удален", response.getBody().getMessage());
        verify(friendsRepository, times(1)).delete(friends);
    }

    @Test
    public void testAllFriends() throws SQLException, IOException {
        User userOwner = new User(1L, "Danila", "321@gmail.com", "2002");
        User userFriend = new User(2L, "Danila2", "321@gmail.com2", "2002");
        InfoUser owner = new InfoUser(1L, "22.02.22", "username", "aboutMe", "123", "men", userOwner);
        InfoUser friend = new InfoUser(2L, "22.02.22", "username", "aboutMe", "123", "men", userFriend);
        Friends friends = new Friends(owner, friend);
        List<Friends> friendsList = Arrays.asList(friends);
        FriendsDto friendsDto = new FriendsDto();
        when(userService.findById(owner.getUserInfo().getId())).thenReturn(Optional.of(userOwner));
        when(infoUserService.findByUserInfo(userOwner.getId())).thenReturn(owner);
        when(friendsMapper.toDTO(friends)).thenReturn(friendsDto);
        List<FriendsDto> response = friendsController.allFriends(userOwner.getId());
        assertEquals(1, response.size());
        assertEquals(friendsDto, response.get(0));
    }

    @Test
    public void testAllSubFriends() {
        User userOwner = new User(1L, "Danila", "321@gmail.com", "2002");
        User userFriend = new User(2L, "Danila2", "321@gmail.com2", "2002");
        InfoUser owner = new InfoUser(1L, "22.02.22", "username", "aboutMe", "123", "men", userOwner);
        InfoUser friend = new InfoUser(2L, "22.02.22", "username", "aboutMe", "123", "men", userFriend);
        Friends friends = new Friends(owner, friend);
        List<Friends> friendsList = Arrays.asList(friends);
        FriendsDto friendsDto = new FriendsDto();
        when(userService.findById(owner.getUserInfo().getId())).thenReturn(Optional.of(userOwner));
        when(infoUserService.findByUserInfo(userOwner.getId())).thenReturn(owner);
        when(friendsMapperTwo.toDTO(friends)).thenReturn(friendsDto);
        List<FriendsDto> response = friendsController.allSubFriends(userOwner.getId());
        assertEquals(1, response.size());
        assertEquals(friendsDto, response.get(0));
    }

}
